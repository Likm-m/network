### SSH登录交换机

##### 实验步骤：

1. 创建拓扑图

2. 给两端设置好IP地址

   1. Pc机可以直接设置

   2. 交换机因为是二层网络设备，要通过给默认的VLAN1这个接口设置ip(设置后，所有的接口都共享一个IP地址)

      ```js
      Switch(config)#interface vlan 1   // 进入vlan1接口
      Switch(config-if)#no shut			// 开启这个接口
      %LINK-5-CHANGED: Interface Vlan1, changed state to up
      %LINEPROTO-5-UPDOWN: Line protocol on Interface Vlan1, changed state to up
      Switch(config-if)#ip address 192.168.10.2 255.255.255.0  // 给vlan1配置ip地址，注意不要忘记子网掩码
      ```

      

3. 修改主机名为默认名称以外的名称

   ```js
   Switch(config)#hostname SW03   // 修改主机名称为SW03
   SW03(config)#  				// 观察提示符已经改变了
   ```

   

4. 给交接机设置域名

   ```js
   SW03(config)#ip domain-name mdd.com  // ip domain-name 域名
   //SW03(config)#no ip domain-lookup  取消默认的域名解析功能，避免在特权模式下，输错命令导致卡死
   
   // 以上两步得到一个完整的域名：SW03.qq.com
   ```

   

5. 创建RSA的KEYS

   ```js
   SW03(config)#crypto key generate rsa   // 以RSA算法生成KEY
   The name for the keys will be: SW03.mdd.com
   Choose the size of the key modulus in the range of 360 to 2048 for your
     General Purpose Keys. Choosing a key modulus greater than 512 may take
     a few minutes.
   
   How many bits in the modulus [512]: 2048   // 密钥长度，最大2048，越大越安全
   % Generating 2048 bit RSA keys, keys will be non-exportable...[OK]
   ```

   

6. 开启SSH的v2版本

   ```js
   SW03(config)#ip ssh version 2  // 以版本2开启SSH功能，前提已经做了3，4，5步
   SW03(config)#line vty 0 15  // 开启虚拟线路 （0-15），这里是开了所有，并进入线路模式
   SW03(config-line)#login local   // 开启本地用户登录模式
   SW03(config-line)#transport input ssh  // 允许SSH协议使用虚拟线路，允许的选项：all 允许所有，none 禁用所有 
   SW03(config-line)#exit  // 退出到全局模式
   SW03(config)#username admin password 123  // 创建本地用户admin并设置密码为123
   ```

   

7. 默认情况下，特权密码是关闭了，且SSH又需要开启，所以在交换机要开启Enable的密码

   ```js
   // SW03(config)#enable password 666  以明文形式设置密码
   SW03(config)#enable secret 777     // 以密文形式设置密码
   SW03(config)#service password-encryption   // 以全局加密的模式对所有密码加密
   ```

   

8. 保存配置

   ```js
   SW03(config)#exit   // 退到特权模式
   SW03#
   %SYS-5-CONFIG_I: Configured from console by console
   SW03#write memory    // 保存设置，简写为w
   Building configuration...
   [OK]
   ```

   

#### 作业

1. 使用SSH登录交换机

![20240430_SSH登陆交换机](./imgs/20240430_SSH登陆交换机.png)

```c#
Switch>en
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#int vlan 1 //进入Vlan1接口
Switch(config-if)#no shut //开启接口

Switch(config-if)#
%LINK-5-CHANGED: Interface Vlan1, changed state to up

%LINEPROTO-5-UPDOWN: Line protocol on Interface Vlan1, changed state to up

Switch(config-if)#ip add 192.168.1.254 255.255.255.0
Switch(config-if)#ip domain-name haimian.com
Switch(config)#hostname sw1
sw1(config)#crypto key generate rsa //以RSA算法生成KEY
The name for the keys will be: sw1.haimian.com
Choose the size of the key modulus in the range of 360 to 2048 for your
  General Purpose Keys. Choosing a key modulus greater than 512 may take
  a few minutes.

How many bits in the modulus [512]: 2048 //密钥长度，最大2048
% Generating 2048 bit RSA keys, keys will be non-exportable...[OK]

sw1(config)#ip ssh version 2
*3月 1 0:8:10.289: %SSH-5-ENABLED: SSH 1.99 has been enabled
sw1(config)#line vty 0 15
sw1(config-line)#login local
sw1(config-line)#transport input ssh 
sw1(config-line)#exit
sw1(config)#username likm password 123
sw1(config)#exit
sw1#
%SYS-5-CONFIG_I: Configured from console by console

sw1#write memory //保存设置
Building configuration...
[OK]
```

- 终端显示

  ![20240430_SSH交换机终端](./imgs/20240430_SSH交换机终端.png)

2. 使用SSH登录路由器

   ![20240430_SSH登录路由器](./imgs/20240430_SSH登录路由器.png)

   - 给PC端设置IP地址+子网掩码+网关

   - ```c#
     
     Router#conf t
     Enter configuration commands, one per line.  End with CNTL/Z.
     Router(config)#int fa 0/0
     Router(config-if)#no shut
     
     Router(config-if)#
     %LINK-5-CHANGED: Interface FastEthernet0/0, changed state to up
     
     %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0, changed state to up
     
     Router(config-if)#ip add 192.168.1.254 255.255.255.0
     Router(config-if)#ip domain-name likm.com
     Router(config)#hostname R1
     R1(config)#cry key g rsa
     The name for the keys will be: R1.likm.com
     Choose the size of the key modulus in the range of 360 to 2048 for your
       General Purpose Keys. Choosing a key modulus greater than 512 may take
       a few minutes.
     
     How many bits in the modulus [512]: 2048
     % Generating 2048 bit RSA keys, keys will be non-exportable...[OK]
     
     R1(config)#ip ssh version 2
     *3月 1 0:6:14.744: %SSH-5-ENABLED: SSH 1.99 has been enabled
     R1(config)#line vty 0 15
     R1(config-line)#login local
     R1(config-line)#transport input ssh
     R1(config-line)#enable password 123
     R1(config)#username likm password 123
     R1(config)#ex
     R1#
     %SYS-5-CONFIG_I: Configured from console by console
     
     R1#w
     Building configuration...
     [OK]
     ```

   - 终端显示

     ![20240430_SSH路由器终端](./imgs/20240430_SSH路由器终端.png)

3. 使用SSH和TELNET登录交换机及路由器

   - 交换机命令

   - ```c#
     SW1(config)#line vty 0 15
     SW1(config-line)#transport input all  //all 所有
     SW1(config-line)#login local 
     ```

   - 路由器命令

   - ```
     R1(config)#line vty 0 15
     R1(config-line)#transport input all 
     R1(config-line)#login local 
     ```

     

