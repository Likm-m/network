#### 计算机网络基础

两个终端，用一条能承载数据传输的物理介质（也称传输媒介）连接起来，组成简单的网络

##### 网络：

1. 至少有两个终端
2. 有一个连接这两个终端的介质
3. 网络是俗称，学名双绞线
   - 光纤
   - 无线：微波、红外线、蓝牙

###### 有了网络之后：

1. 传输数据
2. 有源地址
3. 有目的地址
4. 数据会封装



##### 集线器：

1. 将众多终端相连，形成网络
2. 某终端发给他的数据，会被无脑转发
3. 网络中的终端通过MAP地址识别
   - 每台能上网的设备都必须有MAC地址
   - MAC地址是上网设备一出厂就有的
   - MAC地址：物理地址、以太网地址
   - 全球唯一（DNA）



##### 交换机

交换机连接三台、三台以上终端

1. 很多网络接口
2. 负责同一网络数据的转发
3. 交换机端号都有编号（0/1）
4. 交换机可以学习MAC地址并维护一张表，这个表记录了端口号和MAC的关系

<font color=red>交换机比集线器强，有选择性的</font>

交换机中数据传输的几种方式：

1. 一对一，单播
2. 一对部分，组播
3. 一对any，广播
   - 当目的地址的MAC全都是F（FF-FF-FF-FF-FF-FF）时，就表示广播

交换机：

1. 广播域（同一个交换机互发数据）
2. 局域网核心设备
3. 通过MAC地址识别
4. 交换机可以再接交换机（理论上是无限的）



##### 路由器

1. 隔离广播域（及连通广播域）
2. 维护路由表
3. 用ip识别

<font color=red>路由器也是网关的一种</font>

IP地址的构成：由一系列数字组成，通常分为四个部分，每部分由0到255之间的数字表示，各部分之间用点号（.）分隔。 这四个部分分别代表了网络中的不同层级，如国家、地区、网络和主机

- 网络号+主机号 如：172.168.1.2（前三是网络号，后一是主机号）

- 每个上网设备都要有一个IP才可以跨网连接
- 不同网络访问需要用到路由器
- 通过ip的网络号就可以识别到是不是同一网络



#### 作业：

##### 什么是ARP和什么是RARP？

1. ARP
   - 地址解析协议，即ARP（Address Resolution Protocol），是根据[IP地址](https://baike.baidu.com/item/IP地址/0?fromModule=lemma_inlink)获取[物理地址](https://baike.baidu.com/item/物理地址/2129?fromModule=lemma_inlink)的一个[TCP/IP协议](https://baike.baidu.com/item/TCP/IP协议/0?fromModule=lemma_inlink)。
   - [主机](https://baike.baidu.com/item/主机/455151?fromModule=lemma_inlink)发送信息时将包含目标IP地址的ARP请求广播到局域网络上的所有主机，并接收返回消息，以此确定目标的物理地址；收到返回消息后将该IP地址和物理地址存入本机ARP缓存中并保留一定时间，下次请求时直接查询ARP缓存以节约资源。
   - 地址解析协议是建立在网络中各个主机互相信任的基础上的，局域网络上的主机可以自主发送ARP应答消息，其他主机收到应答报文时不会检测该报文的真实性就会将其记入本机ARP缓存；由此攻击者就可以向某一主机发送伪ARP应答报文，使其发送的信息无法到达预期的主机或到达错误的主机，这就构成了一个[ARP欺骗](https://baike.baidu.com/item/ARP欺骗/0?fromModule=lemma_inlink)。
   - ARP命令可用于查询本机ARP缓存中IP地址和[MAC地址](https://baike.baidu.com/item/MAC地址/0?fromModule=lemma_inlink)的对应关系、添加或删除静态对应关系等。相关协议有[RARP](https://baike.baidu.com/item/RARP/0?fromModule=lemma_inlink)、[代理ARP](https://baike.baidu.com/item/代理ARP/0?fromModule=lemma_inlink)。[NDP](https://baike.baidu.com/item/NDP/0?fromModule=lemma_inlink)用于在[IPv6](https://baike.baidu.com/item/IPv6/0?fromModule=lemma_inlink)中代替地址解析协议。

2. RARP
   - 反向地址转换协议，即RARP（Reverse Address Resolution Protocol）。
   -  反向地址转换协议（RARP）允许局域网的物理机器从网关服务器的 ARP 表或者缓存上请求其 IP 地址。网络管理员在局域网的网关路由器里创建一个表以映射物理地址（MAC）和与其对应的 IP 地址。
   - 当设置一台新的机器时，其 RARP 客户机程序需要向路由器上的 RARP 服务器请求相应的 IP 地址。假设在路由表中已经设置了一个记录，RARP 服务器将会返回 IP 地址给机器，此机器就会存储起来以便日后使用。
   -  RARP 可以使用于以太网、光纤分布式数据接口及令牌环 LAN等。

##### 预习OSI七层模型

![](https://pic3.zhimg.com/v2-0061badffc3a79c9e540bf0189b2647a_r.jpg)



![](https://pic4.zhimg.com/v2-363018760274c3fc551392010f8f3187_r.jpg)

###### OSI中的层 功能 TCP/IP协议族：

1. 应用层 文件传输，电子邮件，文件服务，虚拟终端 TFTP，HTTP，SNMP，FTP，SMTP，DNS，Telnet

2. 表示层 数据格式化，代码转换，数据加密 没有协议

3. 会话层 解除或建立与别的接点的联系 没有协议

4. 传输层 提供端对端的接口 TCP，UDP

5. 网络层 为数据包选择路由 IP，ICMP，RIP，OSPF，BGP，IGMP

6. 数据链路层 传输有地址的帧以及错误检测功能 SLIP，CSLIP，PPP，ARP，RARP，MTU

7. 物理层 以二进制数据形式在物理媒体上传输数据 ISO2110，IEEE802，IEEE802.2